﻿using System.IO;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

using CompleteLibrary.Interfaces;
using Complete.Controllers;

namespace CompleteTest
{
    public class FileTest
    {
        [Fact(DisplayName = "ReturnFile_GivenPath")]
        public void ReturnFile_GivenPath()
        {
            string filePath = "test.png";
            byte[] fileTestByteArray = File.ReadAllBytes(filePath);

            Mock<IFileInterface> fileDataMock = new Mock<IFileInterface>();
            fileDataMock.Setup(x => x.GetImage(filePath))
                        .Returns(fileTestByteArray);

            FileController fileController = new FileController(fileDataMock.Object);
            FileContentResult fileResult = fileController.Get(filePath) as FileContentResult;

            Assert.IsType<FileContentResult>(fileResult);
            Assert.Equal(fileTestByteArray, fileResult.FileContents);
            fileDataMock.Verify(x => x.GetImage(filePath), Times.Once);
        }

        [Fact(DisplayName = "ReturnFile_GivenInvalidPath")]
        public void ReturnFile_GivenInvalidPath()
        {
            string filePath = "test.png";
            byte[] fileTestByteArray = File.ReadAllBytes(filePath);

            Mock<IFileInterface> fileDataMock = new Mock<IFileInterface>();
            fileDataMock.Setup(x => x.GetImage(filePath))
                        .Returns(fileTestByteArray);

            FileController fileController = new FileController(fileDataMock.Object);
            FileContentResult fileResult = fileController.Get("invalid") as FileContentResult;

            Assert.IsType<FileContentResult>(fileResult);
            Assert.NotEqual(fileTestByteArray, fileResult.FileContents);
            fileDataMock.Verify(x => x.GetImage("invalid"), Times.Once);
        }
    }
}
