using System;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;
using Moq;
using Xunit;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

using static CompleteLibrary.Models.MovieModel;
using Complete.Controllers;
using CompleteLibrary.Interfaces;
using System.Linq;

namespace CompleteTest
{
    public class MoviesTest
    {
        private readonly ILogger<MovieController> _logger;

        private Mock<IFormFile> MockImageFile()
        {
            Mock<IFormFile> fileMock = new Mock<IFormFile>();
            FileInfo physicalFile = new FileInfo("test.png");
            MemoryStream ms = new MemoryStream();
            StreamWriter writer = new StreamWriter(ms);
            writer.Write(physicalFile.OpenRead());
            writer.Flush();
            ms.Position = 0;
            string fileName = physicalFile.Name;
            //Setup mock file using info from physical file
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(ms.Length);
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.ContentDisposition).Returns(string.Format("inline; filename={0}", fileName));
            return fileMock;
        }

        [Fact(DisplayName = "ReturnMovieData")]
        public async void ReturnMovieData()
        {
            IEnumerable<Movie> movieTestObject = new List<Movie>();
            movieTestObject.Append(new Movie()
            {
                Id = 1,
                Title = "Test Movie",
                Genre = "Test",
                ReleaseDate = DateTime.Now,
                Image = "./images/test"
            });
            movieTestObject.Append(new Movie()
            {
                Id = 2,
                Title = "Test Movie2",
                Genre = "Test2",
                ReleaseDate = DateTime.Now,
                Image = "./images/test2"
            });

            Mock<IMovieInterface> movieDataMock = new Mock<IMovieInterface>();
            movieDataMock.Setup(x => x.GetMovies())
                .Returns(Task.FromResult(movieTestObject));

            MovieController movieController = new MovieController(_logger, movieDataMock.Object);

            IEnumerable<Movie> movieResult = await movieController.Get();

            movieDataMock.Verify(x => x.GetMovies(), Times.Once);
            Assert.Equal(movieTestObject, movieResult);
        }

        [Fact(DisplayName = "AddMovieTest_WhenModelStateIsValid")]
        public async Task AddMovieTest_WhenModelStateIsValid()
        {
            //Arrange
            Movie movieTestObject = new Movie()
            {
                Id = 1,
                Title = "Test Movie",
                Genre = "Test",
                ReleaseDate = DateTime.Now,
                Image = "./images/test",
                Price = 55
            };
            Mock<IFormFile> mockTestFile = MockImageFile();
            string movieTestString = JsonConvert.SerializeObject(movieTestObject);

            //Act
            Mock<IMovieInterface> movieDataMock = new Mock<IMovieInterface>();
            movieDataMock.Setup(x => x.AddMovie(movieTestObject, mockTestFile.Object))
                .Returns(Task.FromResult(movieTestObject))
                .Verifiable();

            MovieController movieController = new MovieController(_logger, movieDataMock.Object);

            OkObjectResult movieResult = await movieController.Post(movieTestString, mockTestFile.Object) as OkObjectResult;

            //Assert
            Assert.IsType<OkObjectResult>(movieResult);
        }

        [Fact(DisplayName = "AddMovieTest_WhenModelStateIsInValid")]
        public async Task AddMovieTest_WhenModelStateIsInValid()
        {
            Movie movieTestObject = new Movie()
            {
                Id = 1,
                Title = "Test Movie",
                Genre = "Test",
                ReleaseDate = DateTime.Now,
                Image = "./images/test"
            };
            Mock<IFormFile> mockTestFile = MockImageFile();
            string movieTestString = JsonConvert.SerializeObject(movieTestObject);


            Mock<IMovieInterface> movieDataMock = new Mock<IMovieInterface>();
            movieDataMock.Setup(x => x.AddMovie(movieTestObject, mockTestFile.Object))
                .Returns(Task.FromResult(movieTestObject))
                .Verifiable();

            MovieController movieController = new MovieController(_logger, movieDataMock.Object);

            movieController.ModelState.AddModelError("Title", "Required");

            IActionResult movieResult = await movieController.Post(movieTestString, mockTestFile.Object);


            Assert.IsType<BadRequestResult>(movieResult);
            movieDataMock.Verify(x => x.AddMovie(movieTestObject, mockTestFile.Object), Times.Never);
        }
    }
}
