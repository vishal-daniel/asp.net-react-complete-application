import * as WeatherForecasts from './WeatherForecasts';
import * as Counter from './Counter';
import * as Movies from "./Movies";

export interface ApplicationState {
    counter: Counter.CounterState | undefined;
    weatherForecasts: WeatherForecasts.WeatherForecastsState | undefined;
    movies : Movies.MoviesState;
}

export const reducers = {
    counter: Counter.reducer,
    weatherForecasts: WeatherForecasts.reducer,
    movies: Movies.reducer
};

export interface AppThunkAction<TAction> {
    (dispatch: (action: TAction) => void, getState: () => ApplicationState): void;
}
