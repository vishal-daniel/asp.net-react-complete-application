import { Action, Reducer } from "redux";
import axios from "axios";
import { AppThunkAction } from "./";

export type MoviesState = {
  isLoading: boolean;
  movies: Movie[];
  page: number;
  uploadProgress: number;
};

export type Movie = {
  id: number;
  title: string;
  date: string;
  genre: string;
  price: number;
  image: string;
  releaseDate: string;
};

export type AddMovie = {
  Title: string;
  Genre: string;
  Price: number;
  ReleaseDate: string;
};

export type RequestMoviesAction = {
  type: "REQUEST_MOVIES";
};
export type ReceiveMoviesAction = {
  type: "RECEIVE_MOVIES";
  movies: Movie[];
  page: number;
};
export type UploadMovieAction = {
  type: "UPLOAD_MOVIE";
  uploadProgress: number;
};
export type AddMovieAction = {
  type: "ADD_MOVIE";
  movie: Movie;
};
export type DeleteMovieAction = {
  type: "DELETE_MOVIE";
  movies: Movie[];
};

export type FetchMoviesAction = ReceiveMoviesAction | RequestMoviesAction;
export type AddNewMoviesAction = UploadMovieAction | AddMovieAction;

export const actionCreators = {
  getMovies: (): AppThunkAction<FetchMoviesAction> => (dispatch, getState) => {
    dispatch({ type: "REQUEST_MOVIES" });
    let currentPage = getState().movies.page + 1;
    axios
      .get(`/api/movie/${currentPage}`)
      .then((response) => response.data as Promise<Movie[]>)
      .then((data) => {
        dispatch({ type: "RECEIVE_MOVIES", movies: data, page: currentPage });
      });
  },

  addMovies: (
    movie: AddMovie,
    file: File
  ): AppThunkAction<AddNewMoviesAction> => (dispatch, getState) => {
    try {
      const config = {
        onUploadProgress: (progressEvent: ProgressEvent) => {
          let percentCompleted = Math.round(
            (progressEvent.loaded * 100) / progressEvent.total
          );
          dispatch({ type: "UPLOAD_MOVIE", uploadProgress: percentCompleted });
        },
      };
      const formdata = new FormData();
      formdata.append("file", file!);
      formdata.append("movie", JSON.stringify(movie));
      return axios.post("api/movie", formdata, config).then((response) => {
        response.data.image = "api/file/" + response.data.image;
        dispatch({ type: "ADD_MOVIE", movie: response.data });
        dispatch({ type: "UPLOAD_MOVIE", uploadProgress: 0 });
      });
    } catch (e) {
      throw e;
    }
  },

  deleteMovie: (id: number): AppThunkAction<DeleteMovieAction> => (
    dispatch,
    getState
  ) => {
    axios
      .delete(`/api/movie/${id}`)
      .then((response) => {
        const movies = getState().movies.movies.filter(
          (movie) => movie.id !== response.data
        );
        dispatch({ type: "DELETE_MOVIE", movies });
      })
      .catch((reason) => {
        console.error(reason);
      });
  },
};

const initialState: MoviesState = {
  isLoading: true,
  movies: [],
  page: -1,
  uploadProgress: 0,
};

export const reducer: Reducer<MoviesState> = (
  state: MoviesState = initialState,
  incomingAction: Action
) => {
  const action = incomingAction as
    | RequestMoviesAction
    | ReceiveMoviesAction
    | UploadMovieAction
    | AddMovieAction
    | DeleteMovieAction;

  switch (action.type) {
    case "REQUEST_MOVIES":
      return {
        movies: state.movies,
        isLoading: true,
        page: state.page,
        uploadProgress: state.uploadProgress,
      };

    case "RECEIVE_MOVIES":
      return {
        movies: state.movies.concat(action.movies),
        isLoading: false,
        page: action.page,
        uploadProgress: state.uploadProgress,
      };

    case "UPLOAD_MOVIE":
      return {
        movies: state.movies,
        isLoading: false,
        page: state.page,
        uploadProgress: action.uploadProgress,
      };

    case "ADD_MOVIE":
      return {
        movies: state.movies.concat(action.movie),
        isLoading: false,
        page: state.page,
        uploadProgress: state.uploadProgress,
      };

    case "DELETE_MOVIE":
      return {
        movies: action.movies,
        isLoading: false,
        page: state.page,
        uploadProgress: state.uploadProgress,
      };

    default:
      return state;
  }
};
