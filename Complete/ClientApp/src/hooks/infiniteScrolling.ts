import { useCallback, useEffect } from "react";

// infinite scrolling with intersection observer
export const useInfiniteScroll = (scrollRef:React.MutableRefObject<null>, func:Function) => {
    const scrollObserver = useCallback(
      node => {
        new IntersectionObserver(entries => {
          entries.forEach(entry => {
            if (entry.isIntersecting) {
              func()
            }
          });
        }).observe(node);
      },
      [func]
    );
    useEffect(() => {
      if (scrollRef.current) {
        scrollObserver(scrollRef.current);
      }
    }, [scrollObserver, scrollRef]);
  }
  