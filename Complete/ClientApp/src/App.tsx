import * as React from "react";
import { Route, Redirect } from "react-router";
import Layout from "./components/Layout";

import Counter from "./components/Counter";
import FetchData from "./components/FetchData";
import Movies from "./components/Movies/Movies";

import "./custom.css";

export default () => (
  <Layout>
    <Route exact path="/">
      <Redirect to="/movies" />
    </Route>
    <Route path="/counter" component={Counter} />
    <Route path="/fetch-data/:startDateIndex?" component={FetchData} />
    <Route path="/movies" component={Movies} />
  </Layout>
);
