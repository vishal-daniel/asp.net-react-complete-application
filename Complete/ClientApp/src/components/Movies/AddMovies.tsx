import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import { Button, Progress } from "reactstrap";

import * as MoviesStore from "../../store/Movies";
import { AppThunkAction } from "../../store";

type AddMovies = {
  addMovies: (
    movie: MoviesStore.AddMovie,
    file: File
  ) => AppThunkAction<MoviesStore.AddNewMoviesAction>;
  uploadProgress: number;
};

const AddMovies = (props: AddMovies) => {
  const history = useHistory();
  const [Title, setTitle] = useState<string>("");
  const [ReleaseDate, setReleaseDate] = useState<string>("");
  const [Genre, setGenre] = useState<string>("");
  const [Price, setPrice] = useState<number>(0);
  const [Image, setImage] = useState<File>();
  const [isUploading, setIsUploading] = useState(false);

  const handleSubmit = (e: React.FormEvent) => {
    setIsUploading(true);
    e.preventDefault();
    e.stopPropagation();
    Promise.resolve(
      props.addMovies({ Price, Title, Genre, ReleaseDate }, Image!)
    )
      .then(() => {
        history.push("/movies");
      })
      .catch((e) => {
        alert("error");
        console.error(e);
      });
  };

  return (
    <div className="row">
      <form className="col s12" onSubmit={handleSubmit}>
        <div className="input-field col s12">
          <input
            id="Title"
            type="text"
            className="validate"
            onChange={(e) => {
              setTitle(e.target.value);
            }}
          />
          <label htmlFor="Title">Title</label>
        </div>
        <div className="input-field col s12">
          <input
            id="ReleaseDate"
            type="date"
            className="validate"
            onChange={(e) => {
              setReleaseDate(e.target.value);
            }}
          />
          <label htmlFor="ReleaseDate">Release Date</label>
        </div>
        <div className="input-field col s12">
          <input
            id="Genre"
            type="text"
            className="validate"
            onChange={(e) => {
              setGenre(e.target.value);
            }}
          />
          <label htmlFor="Genre">Genre</label>
        </div>
        <div className="input-field col s12">
          <input
            id="Price"
            type="number"
            className="validate"
            onChange={(e) => {
              setPrice(parseInt(e.target.value));
            }}
          />
          <label htmlFor="Price">Price</label>
        </div>
        <div className="input-field col s12">
          <input
            required
            id="file"
            type="file"
            className="validate"
            accept="image/*"
            onChange={(e) => {
              setImage(e.target.files![0]);
            }}
          />
        </div>
        <Button onSubmit={handleSubmit}>Submit</Button>
        {isUploading && (
          <Progress value={props.uploadProgress}>
            {props.uploadProgress}%
          </Progress>
        )}
      </form>
    </div>
  );
};

export default AddMovies;
