﻿import React from "react";
import { connect } from "react-redux";
import { useRouteMatch, Switch, Route } from "react-router-dom";

import AddMovies from "./AddMovies";
import ViewMovies from "./ViewMovies";
import { ApplicationState } from "../../store";
import * as MoviesStore from "../../store/Movies";

type MoviesProps = MoviesStore.MoviesState & typeof MoviesStore.actionCreators;

const Movies = (props: MoviesProps) => {
  const path = useRouteMatch().path;
  return (
    <Switch>
      <Route exact path={path}>
        <ViewMovies
          movies={props.movies}
          isLoading={props.isLoading}
          getMovies={props.getMovies}
          deleteMovie={props.deleteMovie}
        />
      </Route>
      <Route path={`${path}/add/:id?`}>
        <AddMovies
          addMovies={props.addMovies}
          uploadProgress={props.uploadProgress}
        />
      </Route>
    </Switch>
  );
};

export default connect(
  (state: ApplicationState) => state.movies,
  MoviesStore.actionCreators
)(Movies as React.FunctionComponent);
