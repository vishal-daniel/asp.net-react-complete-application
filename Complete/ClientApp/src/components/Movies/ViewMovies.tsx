import React, { useRef } from "react";
import { Link, useRouteMatch, useHistory } from "react-router-dom";
import styled from "styled-components";
import { Button, Table, Spinner } from "reactstrap";

import { AppThunkAction } from "../../store";
import * as MoviesStore from "../../store/Movies";
import { useInfiniteScroll } from "../../hooks/infiniteScrolling";
import { useLazyLoading } from "../../hooks/lazyLoading";
import placeholder from "../../assets/images/placeholder.png";

const StyledButton = styled(Button)`
  margin: auto;
  width: fit-content;
  display: block;
`;

const StyledSpinner = styled(Spinner)`
  margin: auto;
  display: block;
`;

type ViewMoviesProps = {
  isLoading: boolean;
  movies: MoviesStore.Movie[];
  getMovies: () => AppThunkAction<MoviesStore.FetchMoviesAction>;
  deleteMovie: (id: number) => AppThunkAction<MoviesStore.DeleteMovieAction>;
};

const ViewMovies = (props: ViewMoviesProps) => {
  const url = useRouteMatch().url;
  const history = useHistory();
  const bottomBoundaryRef = useRef(null);

  useInfiniteScroll(bottomBoundaryRef, props.getMovies);
  useLazyLoading(".movie_image", props.movies);

  return (
    <>
      <Table>
        <thead>
          <tr>
            <th>Image</th>
            <th>Title</th>
            <th>Release Date</th>
            <th>Genre</th>
            <th>Price</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {props.movies.map((movie: MoviesStore.Movie) => (
            <tr
              key={movie.id}
              onDoubleClick={() => {
                history.push(`/movies/add/${movie.id}`);
              }}
            >
              <th scope="row">
                <img
                  className="movie_image"
                  src={placeholder}
                  data-src={movie.image}
                  alt="thumbnail"
                  width="60px"
                  height="60px"
                />
              </th>
              <td>{movie.title}</td>
              <td>{movie.date}</td>
              <td>{movie.genre}</td>
              <td>{movie.price}</td>
              <td>
                <Button
                  color="danger"
                  onClick={() => {
                    props.deleteMovie(movie.id);
                  }}
                >
                  Delete
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      {props.isLoading && <StyledSpinner type="grow" color="secondary" />}

      <div id="page-bottom-boundary" ref={bottomBoundaryRef} />
      <StyledButton tag={Link} to={`${url}/add`} color="primary">
        Add Movie
      </StyledButton>
    </>
  );
};

export default ViewMovies;
