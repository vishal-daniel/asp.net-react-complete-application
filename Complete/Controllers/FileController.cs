﻿using System;
using CompleteLibrary.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Complete.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly IFileInterface _fileData;
        public FileController(IFileInterface fileData)
        {
            _fileData = fileData;
        }

        [HttpGet("{imageName}")]
        public IActionResult Get([FromRoute] string imageName)
        {
            try
            {
                return File(_fileData.GetImage(imageName), "image/*");
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Image not found");
            }
            return NotFound("The requested image was not found");
        }
    }
}