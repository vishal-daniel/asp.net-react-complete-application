﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CompleteLibrary.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using static CompleteLibrary.Models.MovieModel;

namespace Complete.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private readonly ILogger<MovieController> _logger;
        private readonly IMovieInterface _movies;
        public MovieController(ILogger<MovieController> logger, IMovieInterface movies)
        {
            _movies = movies;
            _logger = logger;
        }

        // GET: api/Movie/<page_no>
        [HttpGet("{page}", Name = "Get")]
        public async Task<IEnumerable<Movie>> Get(int page)
        {
            return await _movies.GetMovies(page);
        }

        // POST: api/Movie
        [HttpPost]
        public async Task<IActionResult> Post([FromForm(Name = "movie")]string _movie, [FromForm(Name = "file")]IFormFile file)
        {
            Movie movie = JsonConvert.DeserializeObject<Movie>(_movie);
            try
            {
                if (ModelState.IsValid)
                {
                    await _movies.AddMovie(movie, file);
                    return Ok(movie);
                }
            }
            catch
            {
                return BadRequest();
            }
            return BadRequest();
        }

        // PUT: api/Movie/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                return Ok(await _movies.DeleteMovie(id));
            }
            catch
            {
                return NotFound();
            }
        }

    }
}

