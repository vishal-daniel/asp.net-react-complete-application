### Build

- In `$THIS_REPO/CompleteLibrary` run `dotnet build`

- In `$THIS_REPO/Complete/ClientApp` run `yarn install` or `npm install`

- In `$THIS_REPO/Complete` run `dotnet build`


### Run

- In `$THIS_REPO/Complete` run `dotnet run`