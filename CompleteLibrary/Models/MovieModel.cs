﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace CompleteLibrary.Models
{
    public class MovieModel
    {
        [Bind("Title", "ReleaseDate", "Genre", "Price", "Image")]
        public class Movie
        {
            [Key]
            public int Id { get; set; }
            public string Title { get; set; }
            [DataType(DataType.Date)]
            public DateTime ReleaseDate { get; set; }
            public string Genre { get; set; }
            public double Price { get; set; }
            public string Image { get; set; }
            public string Date => ReleaseDate.ToShortDateString();
        }

        public class MovieDTO
        {
            public int Id { get; set; }
            public string Title { get; set; }
            [DataType(DataType.Date)]
            public DateTime ReleaseDate { get; set; }
            public string Genre { get; set; }
            public double Price { get; set; }
            public string Image { get; set; }
            public string ImagePath { get; set; }
            public string Date => ReleaseDate.ToShortDateString();
        }
    }
}
