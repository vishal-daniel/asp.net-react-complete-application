﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using CompleteLibrary.Interfaces;

namespace CompleteLibrary.Data
{
    public class FileData : IFileInterface
    {
        private string ImagePath
        {
            get { return Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"..", "Data", "Images")); }
        }
        //Set Compression level : 0L(highest compression) - 100L (lowest compression)
        private readonly long compressionLevel = 10L;

        private ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        private bool ThumbnailCallback()
        {
            return false;
        }

        public byte[] GetImage(string imageName)
        {
            if (imageName is null)
            {
                return null;
            }

            try
            {
                using (Bitmap bmp = new Bitmap($"{ImagePath}/{imageName}"))
                {
                    //// ----- Compress Image -----
                    ImageCodecInfo encoderType = GetEncoder(ImageFormat.Jpeg);
                    Encoder encoder = Encoder.Quality;
                    using (EncoderParameters encoderParameters = new EncoderParameters(1))
                    {
                        using (EncoderParameter encoderParameter = new EncoderParameter(encoder, compressionLevel))
                        {
                            encoderParameters.Param[0] = encoderParameter;
                            using (MemoryStream compressedImage = new MemoryStream())
                            {
                                bmp.Save(compressedImage, encoderType, encoderParameters);
                                return compressedImage.ToArray();
                            }
                        }
                    }
                    //// ----- Compress Image ------


                    ////------ Thumbnail ------
                    //Image.GetThumbnailImageAbort myCallback = new Image.GetThumbnailImageAbort(ThumbnailCallback);
                    //using (Image image = bmp.GetThumbnailImage(120, 120, myCallback, System.IntPtr.Zero))
                    //{
                    //    using (MemoryStream compressedImage = new MemoryStream())
                    //    {
                    //        image.Save(compressedImage, bmp.RawFormat);
                    //        return compressedImage.ToArray();
                    //    }
                    //}
                    ////------ Thumbnail ------
                }
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Image not found.");
            }

            return null;
        }
    }
}
