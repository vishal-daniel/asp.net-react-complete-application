﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using CompleteLibrary.Connection;
using CompleteLibrary.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using static CompleteLibrary.Models.MovieModel;

namespace CompleteLibrary.Data
{
    public class MovieData : IMovieInterface
    {
        private readonly DBConnectionContext _dBConnectionContext;
        private string ImagePath
        {
            get { return Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"..", "Data", "Images")); }
        }

        public MovieData(DBConnectionContext dBConnectionContext)
        {
            _dBConnectionContext = dBConnectionContext;
        }

        public async Task<IEnumerable<Movie>> GetMovies(int page)
        {
            List<Movie> movies = await _dBConnectionContext.Movies.Skip(10 * page).Take(10).ToListAsync();
            Parallel.ForEach(movies, (movie => movie.Image = ($@"api/file/{movie.Image}")));
            return movies;
        }

        public async Task AddMovie(Movie movie, IFormFile file)
        {
            try
            {
                if (file.Length > 0)
                {
                    string fileName = $"{movie.Title}_{DateTime.Now}{Path.GetExtension(file.FileName)}"
                                        .Replace(" ", "_")
                                        .Replace("/", "_")
                                        .Replace(":", "-");
                    using (FileStream stream = new FileStream($@"{ImagePath}\{fileName}", FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                    movie.Image = fileName;
                    _dBConnectionContext.Add(movie);
                    await _dBConnectionContext.SaveChangesAsync();
                }
            }
            catch (DbUpdateException error)
            {
                throw error;
            }
        }

        public async Task<int> DeleteMovie(int id)
        {
            try
            {
                Movie movie = await _dBConnectionContext.Movies.FindAsync(id);
                File.Delete($@"{ImagePath}\{movie.Image}");
                _dBConnectionContext.Movies.Remove(movie);
                await _dBConnectionContext.SaveChangesAsync();
                return id;
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException error)
            {
                throw error;
            }
        }
    }
}
