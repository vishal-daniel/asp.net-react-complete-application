﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using static CompleteLibrary.Models.MovieModel;

namespace CompleteLibrary.Interfaces
{
    public interface IMovieInterface
    {
        Task<IEnumerable<Movie>> GetMovies(int page);
        Task AddMovie(Movie movie, IFormFile file);
        Task<int> DeleteMovie(int id);
    }
}
