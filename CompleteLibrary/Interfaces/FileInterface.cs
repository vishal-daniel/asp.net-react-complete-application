﻿namespace CompleteLibrary.Interfaces
{
    public interface IFileInterface
    {
        byte[] GetImage(string imageName);
    }
}
